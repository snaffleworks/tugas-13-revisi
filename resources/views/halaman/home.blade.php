@extends('layout.master')
@section('judul')
    Media Online
@endSection
@section('content')

    <h1>Media Online</h1>
    <h3>Sosial Media Developer</h3>
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    </div>  
    <div>
        <p><b>Benefit Join di Media Online</b></p> 
            <Ul>
                <li>Mendapatkan motivasi dari sesama Developer</li>
                <li>Sharing Knowledge</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </Ul>
    </div>
    <div>
        <p><b>Cara Bergabung ke Media Online</b></p>
            <ol>
                <li>Mengunjungi Website ini</li> 
                <li>Mendaftarkan di <a href="/register">form Sign Up</a></li> 
                <li>Selesai</li>
            </ol>
    </div>
@endsection